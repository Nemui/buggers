﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public Collider ground;
	public GameObject placeable;
	
	
	void Start () {
	
	}
	
	//If clicked ground, place "placeable"
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (ground.Raycast(ray, out hit, 100f))
			{
				Instantiate(placeable, hit.point, Quaternion.identity);
			}
		}
	}
}
