﻿using UnityEngine;
using System.Collections;

public static class Utilities {
	public static Vector3 Vector2To3Floor(Vector2 vec2)
	{
		return new Vector3(vec2.x, 0.0f, vec2.y);
	}
}
