﻿using UnityEngine;
using System.Collections;

public class Genome {
	public float decisionKeeping = 0.1f;
	public float maxFood = 1.0f;
	public float maxHealt = 1.0f;
	public float foodConsumption = 1.0f;
	public float starvingSpeed = 0.05f;
	public float touchRange = 0.5f;
	public float sniffRange = 2.0f;
	public float sniffPrecision = 0.5f;

	public float movementSpeed = 1.0f;
	//public float turningSpeed = 1.0f;

	public float hungryFactor = 1.0f;
}
