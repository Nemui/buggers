﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BugCore : WorldObject {

	public Genome genome = new Genome();
	public float Healt { get; set; }

	
	BugMind bugMind;
	Nullable<Vector3> movementTarget = null;


	public bool IsMoving { get { return movementTarget.HasValue; } }
	
	public void MoveTo(Vector3 target)
	{
		movementTarget = target;
	}

	// Use this for initialization
	void Start () {
		Healt = genome.maxHealt;
		bugMind = new BugMind(this);
	}

	void Update () {
		//Check is alive
		if (Healt <= 0.0f)
		{
			Destroy(this.gameObject);
			return;
		}

		List<Perception> perceptions = GetPerceptions();

		//Update decision making (bugMind uses custom time parameter since it will not call at every frame at future)
		bugMind.Update(new GameTime() { TotalTime = Time.time, DeltaTime = Time.deltaTime }, perceptions);

		//Update movement (note that actual move order is called from bugMind or deeper)
		if (movementTarget.HasValue)
		{
			Vector3 toTarget = movementTarget.Value - transform.position;
			float distance = toTarget.magnitude;
			float frameMoveDistance = Time.deltaTime * genome.movementSpeed;

			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, 90.0f - Mathf.Rad2Deg * Mathf.Atan2(toTarget.z, toTarget.x) , 0.0f), /*turningSpeed * Time.deltaTime*/ 0.1f);

			if (distance <= frameMoveDistance)
			{
				transform.position = movementTarget.Value;
				movementTarget = null;
			}
			else
			{
				//transform.Translate(toTarget * (frameMoveDistance / distance));
				transform.Translate(new Vector3(0.0f , 0.0f, frameMoveDistance));
			}


		}
	}

	private static LayerMask perceptionLayer = 1 << LayerMask.NameToLayer("WorldObject");
	protected List<Perception> GetPerceptions()
	{
		List<Perception> result = new List<Perception>();

		Collider[] colliders = Physics.OverlapSphere(transform.position, Mathf.Max(genome.sniffRange, genome.touchRange), perceptionLayer);
		foreach(Collider collider in colliders)
		{
			WorldObject wo = collider.GetComponent<WorldObject>();
			float distance = Vector3.Distance(transform.position, wo.transform.position);
			float positionError;
			if (distance < genome.touchRange)
				result.Add(new Perception(wo, 0.0f, Perception.Type.Touch));
			else if (distance < genome.sniffRange)
				result.Add(new Perception(wo, distance * (1.0f - genome.sniffPrecision), Perception.Type.Sniff));
		}

		return result;
	}



}
