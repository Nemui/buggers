﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Instinct {

	protected BugCore bugCore;

	protected Instinct(BugCore bugCore)
	{
		this.bugCore = bugCore;
	}

	public abstract float UpdateSituation(GameTime time, List<Perception> perceptions);
	public abstract void Perform(GameTime time, List<Perception> perceptions);
}
