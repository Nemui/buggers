﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BugMind {
	
	private List<Instinct> instincts = new List<Instinct>();
	private Instinct currentInstincts = null;

	private BugCore bugCore;

	public BugMind(BugCore bugCore)
	{
		this.bugCore = bugCore;
		instincts.Add(new Hungry(bugCore));
	}

	public void Update (GameTime time, List<Perception> perceptions) {

		float needMax = float.MinValue;
		Instinct mostNeeded = null;
		foreach(Instinct instinct in instincts)
		{
			if (instinct == currentInstincts)
				continue;

			float need = instinct.UpdateSituation(time, perceptions);
			if (need > needMax)
			{
				needMax = need;
				mostNeeded = instinct;
			}
		}

		if (currentInstincts == null || needMax * (1.0f - bugCore.genome.decisionKeeping) > currentInstincts.UpdateSituation(time, perceptions))
		{
			currentInstincts = mostNeeded;
		}

		currentInstincts.Perform(time, perceptions);

	}
}
