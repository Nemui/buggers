﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hungry : Instinct {

	float food;

	public Hungry(BugCore bugCore) : base(bugCore)
	{
		food = bugCore.genome.maxFood;
	}

	public override float UpdateSituation(GameTime time, List<Perception> perceptions)
	{
		food -= time.DeltaTime * bugCore.genome.foodConsumption;

		if (food <= 0.0f)
		{
			food = 0.0f;
			bugCore.Healt -= time.DeltaTime * bugCore.genome.starvingSpeed;
		}

		return  food * bugCore.genome.hungryFactor / bugCore.genome.maxFood;
	}

	public override void Perform (GameTime time, List<Perception> perceptions) {

		Perception nearest = null;
		float nearestDistance = float.MaxValue;
		foreach (Perception perception in perceptions)
		{
			float distance = Vector3.Distance(perception.worldObject.transform.position, bugCore.transform.position);
			if (distance < nearestDistance)
			{
				nearestDistance = distance;
				nearest = perception;
			}
		}

		Vector3 target;
		if (nearest != null)
		{
			if (nearest.type == Perception.Type.Sniff)
			{
				bugCore.MoveTo(nearest.worldObject.transform.position);
			}
			else if (nearest.type == Perception.Type.Touch)
			{
				Eat(nearest.worldObject);
			}
		}
		else
		{
			if (!bugCore.IsMoving)
				bugCore.MoveTo(Utilities.Vector2To3Floor(Random.insideUnitCircle * 10.0f));
		}

	}

	public void Eat(WorldObject obj)
	{
		float foodNeed = bugCore.genome.maxFood - food;
		float foodAvailable = obj.ConsistOfEnergy;

		if (foodNeed >= foodAvailable)
		{
			food += foodAvailable;
			obj.ConsistOfEnergy = 0.0f;
		}
		else
		{
			food += foodNeed;
			obj.ConsistOfEnergy -= foodNeed;
		}
	}
}
