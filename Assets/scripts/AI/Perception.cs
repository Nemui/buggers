﻿using UnityEngine;
using System.Collections;

public class Perception {

	public enum Type
	{
		Touch,
		Sniff,
	}

	public WorldObject worldObject;
	public float positionError;
	public Type type;

	public Perception(WorldObject obj, float posError, Type perceptionType)
	{
		worldObject = obj;
		positionError = posError;
		type = perceptionType;
	}

}

/*public class Touched : Perception {
	public WorldObject touched;
}

public class Sniffed : Perception {
	public WorldObject touched;
}*/
