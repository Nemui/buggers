﻿using UnityEngine;
using System.Collections;

public class Nest : MonoBehaviour {
		
	public GameObject bugPrefab;
	public float bugCreateTime = 1.0f;
	
	private float lastBugTime;
	
	void Start () {
		lastBugTime = Time.time;
	}
	
	//Creat one new bug once per bugCreateTime
	void Update () {
		if (Time.time - lastBugTime >= bugCreateTime)
		{
			Instantiate(bugPrefab, transform.position, Quaternion.identity);
			lastBugTime = Time.time;
		}
	}
}
