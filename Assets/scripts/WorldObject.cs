﻿using UnityEngine;
using System.Collections;

public class WorldObject : MonoBehaviour {
	public float ConsistOfEnergy { get; set; }

	void Update()
	{
		if (ConsistOfEnergy <= 0.0f)
		{
			Destroy(this.gameObject);
		}
	}
}
