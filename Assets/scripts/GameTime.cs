﻿using UnityEngine;
using System.Collections;

public class GameTime {

	public float DeltaTime { get; set; }
	public float TotalTime { get; set; }

}
